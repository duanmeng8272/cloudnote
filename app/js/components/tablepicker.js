define([
    'js/resolve',
    'angular-ui-bootstrap'
], function(resolve) {
    angular.module('app.tablepicker', ['ui.bootstrap'])
        .directive('tablepicker', tablePickerDirective);

    function tablePickerDirective() {
        return resolve({
            restrict: 'E',
            replace: true,
            templateUrl: 'js/templates/tablepicker.html',
            scope: {
                position: '=',
                onSelected: '<'
            },
            link: function(scope) {
                scope.rows = [];
                scope.columns = [];
                scope.selectedRow = 1;
                scope.selectedColumn = 1;
                for (var i = 1; i <= 8; i++) {
                    scope.rows.push(i);
                    scope.columns.push(i);
                }
                //
                scope.render = function(row, column) {
                    scope.selectedRow = row;
                    scope.selectedColumn = column;
                }
                //
                scope.select = function(row, column) {
                    if (typeof(scope.onSelected) === 'function') {
                        scope.onSelected(row, column);
                    }
                }
                //
                scope.toggle = function(open) {
                    if (open) {
                        scope.selectedRow = 1;
                        scope.selectedColumn = 1;
                    }
                }
            }
        });
    }
});
