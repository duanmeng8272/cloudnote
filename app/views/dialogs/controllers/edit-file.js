define(['app'], function(app) {
    app.controller('editFile', function($scope, $http, $uibModalInstance, toastr, _context, _dialog) {
        var self = this;

        self.parent = '';
        self.origin = '';
        self.module = '';
        self.file = '';
        self.filename = '';
        self.extension = '';
        self.action = '';

        self.options = [{
            value: '', label: '-请选择-'
        }, {
            value: '-'
        }, {
            value: 'java', label: 'Java(*.java)'
        }, {
            value: 'cs', label: 'C#(*.cs)'
        }, {
            value: 'cpp', label: 'C++(*.cpp)'
        }, {
            value: '-'
        }, {
            value: 'py', label: 'Python(*.py)'
        }, {
            value: 'go', label: 'Golang(*.go)'
        }, {
            value: 'groovy', label: 'Groovy(*.groovy)'
        }, {
            value: 'swift', label: 'Swift(*.swift)'
        }, {
            value: '-'
        }, {
            value: 'html', label: 'HTML(*.html)'
        }, {
            value: 'css', label: 'CSS(*.css)'
        }, {
            value: 'js', label: 'JavaScript(*.js)'
        }, {
            value: 'md', label: 'Markdown(*.md)'
        }, {
            value: '-'
        }, {
            value: 'sql', label: 'SQL(*.sql)'
        }, {
            value: 'sh', label: 'Shell(*.sh)'
        }, {
            value: '-'
        }, {
            value: 'xml', label: 'XML(*.xml)'
        }, {
            value: 'yml', label: 'YAML(*.yml)'
        }, {
            value: 'json', label: 'JSON(*.json)'
        }, {
            value: 'txt', label: 'Text(*.txt)'
        }, {
            value: '-'
        }, {
            value: 'sdoc', label: '文档(*.sdoc)'
        }, {
            value: 'hxls', label: '表格(*.hxls)'
        }, {
            value: 'zsvg', label: '图形(*.zsvg)'
        }, {
            value: '-'
        }, {
            value: '*', label: '其他(*.*)'
        }];

        self.init = function() {
         // _context = {parent,file}
            self.origin = _context.file;
            self.file = _context.file;
            self.action = (_context.file ? 'rename' : 'new');

            if (_context.parent) {
                var slashIndex = _context.parent.indexOf('/');
                if (slashIndex > 0) {
                    self.module = _context.parent.substring(0, slashIndex);
                    self.parent = _context.parent.substring(slashIndex + 1);
                } else {
                    self.module = _context.parent;
                    self.parent = '';
                }
            }

            if (_context.file) {
                var filename = _context.file;
                var dotIndex = filename.lastIndexOf('.');
                if (dotIndex == -1) {
                    self.filename = filename;
                    self.extension = '*';
                } else {
                    self.filename = filename.substring(0, dotIndex);
                    self.extension = filename.substring(dotIndex + 1);
                    if (!_.find(self.options, function(option) {
                        return self.extension == option.value;
                    })) {
                        self.filename = filename;
                        self.extension = '*';
                    }
                }
            }

            $scope.$watchGroup(['dvm.filename', 'dvm.extension'], function(newValue) {
                var filename = newValue[0], extension = newValue[1];
                self.file = filename + (extension == '*' ? '' : '.' + extension);
            });
        }

        self.save = function(valid) {
            if (valid) {
                if (_context.parent) {
                    if (_context.file) {
                        $http.post('/note/data/rename', {
                            module: self.module,
                            oldPath: self.parent + '/' + self.origin,
                            newPath: self.parent + '/' + self.file
                        }).then(function(response) {
                            setTimeout(function() {
                                if (response.data == 'required') {
                                    _dialog.alert('提示信息', '模块【' + self.module + '】不存在！');
                                } else if(response.data) {
                                    toastr.success('文件【' + self.origin + '】已修改！');
                                    $uibModalInstance.close(self.file);
                                } else if (response.status == 200) {
                                    xDialog.alert('提示信息', '您重命名的文件已存在！');
                                }
                            }, 500);
                        });
                    } else {
                        $http.post('/note/data/save', {
                            created: true,
                            module: self.module,
                            type: 'file',
                            path: self.parent + '/' + self.file,
                            data: ''
                        }).then(function(response) {
                            setTimeout(function() {
                                if (response.data == 'required') {
                                    _dialog.alert('提示信息', '模块【' + self.module + '】不存在！');
                                } else if (response.data) {
                                    toastr.success('文件【' + self.file +'】已添加！');
                                    $uibModalInstance.close(self.file);
                                } else if (response.status == 200) {
                                    _dialog.alert('提示信息', '您创建的文件已存在！');
                                }
                            }, 500);
                        });
                    }
                } else {
                    $uibModalInstance.close(self.file);
                }
            }
        }

        self.cancel = function() {
            $uibModalInstance.dismiss();
        }
    });
});
