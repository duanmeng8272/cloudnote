define(['app'], function(app) {
    app.controller('editFolder', function($http, $uibModalInstance, toastr, _context, _dialog) {
        var self = this;
        self.origin = '';
        self.module = '';
        self.parent = '';
        self.folder = '';
        self.action = '';

        self.init = function() {
            // _context = {parent,folder}
            self.origin = _context.folder;
            self.folder = _context.folder;
            self.action = (_context.folder ? 'rename' : 'new');
            if (_context.parent) {
                var slashIndex = _context.parent.indexOf('/');
                if (slashIndex > 0) {
                    self.module = _context.parent.substring(0, slashIndex);
                    self.parent = _context.parent.substring(slashIndex + 1);
                } else {
                    self.module = _context.parent;
                    self.parent = '';
                }
            }
        }

        self.save = function(valid) {
            if (valid) {
                if (_context.folder) {
                    $http.post('/note/data/rename', {
                        module: self.module,
                        oldPath: self.parent + '/' + self.origin,
                        newPath: self.parent + '/' + self.folder
                    }).then(function(response) {
                        setTimeout(function() {
                            if (response.data == 'required') {
                                _dialog.alert('提示信息', '模块【' + self.module + '】不存在！');
                            } else if(response.data) {
                                toastr.success('文件夹【' + self.origin + '】已修改！');
                                $uibModalInstance.close(self.folder);
                            } else if (response.status == 200) {
                                xDialog.alert('提示信息', '您重命名的文件夹已存在！');
                            }
                        }, 500);
                    });
                } else {
                    $http.post('/note/data/save', {
                        created: true,
                        module: self.module,
                        type: 'folder',
                        path: self.parent + '/' + self.folder,
                    }).then(function(response) {
                        setTimeout(function() {
                            if (response.data == 'required') {
                                _dialog.alert('提示信息', '模块【' + self.module + '】不存在！');
                            } else if (response.data) {
                                toastr.success('文件夹【' + self.folder + '】已添加！');
                                $uibModalInstance.close(self.folder);
                            } else if (response.status == 200) {
                                _dialog.alert('提示信息', '您创建的文件夹已存在！');
                            }
                        }, 500);
                    });
                }
            }
        }

        self.cancel = function() {
            $uibModalInstance.dismiss();
        }
    });
});
