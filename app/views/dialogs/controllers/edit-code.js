define(['app', 'ace-language-tools'], function(app) {
    app.controller('editCode', function($timeout, _context, $uibModalInstance) {
        var self = this;

        self.options = [{
            value: 'java', label: 'Java', mode: 'java'
        }, {
            value: 'cs', label: 'C#', mode: 'csharp'
        }, {
            value: 'cpp', label: 'C++', mode: 'c_cpp'
        }, {
            value: '-'
        }, {
            value: 'py', label: 'Python', mode: 'python'
        }, {
            value: 'go', label: 'Golang', mode: 'golang'
        }, {
            value: 'groovy', label: 'Groovy', mode: 'groovy'
        }, {
            value: 'swift', label: 'Swift', mode: 'swift'
        }, {
            value: '-'
        }, {
            value: 'html', label: 'HTML', mode: 'html'
        }, {
            value: 'css', label: 'CSS', mode: 'css'
        }, {
            value: 'js', label: 'JavaScript', mode: 'javascript'
        }, {
            value: 'md', label: 'Markdown', mode: 'markdown'
        }, {
            value: '-'
        }, {
            value: 'sql', label: 'SQL', mode: 'sql'
        }, {
            value: 'sh', label: 'Shell', mode: 'sh'
        }, {
            value: '-'
        }, {
            value: 'xml', label: 'XML', mode: 'xml'
        }, {
            value: 'yml', label: 'YAML', mode: 'yaml'
        }, {
            value: 'json', label: 'JSON', mode: 'json'
        }, {
            value: 'txt', label: 'PlainText', mode: 'plain_text'
        }, {
            value: '-'
        }, {
            value: '', label: '其他语言', mode: 'text'
        }];

        var editor = null;

        self.init = function() {
            self.lang = _context.lang || 'java';
            self.code = _context.code || '';
            self.enabled = _context.lang ? false : true;
            self.editable = false;
            //
            editor = ace.edit('source-editor', {
                mode: 'ace/mode/text',
                theme: 'ace/theme/chrome',
                cursorStyle: 'slim',
                fontSize: 14,
                wrap: true,
                useWorker: (_context.checked === true),
                showPrintMargin: false,
                autoScrollEditorIntoView: true,
                enableBasicAutocompletion: true,
                enableSnippets: true,
                enableLiveAutocompletion: true
            });
            editor.setValue(self.code, -1);
            editor.focus();
            editor.on('change', function(e) {
                $timeout(function() {
                    self.code = editor.getValue();
                });
            });
        }

        self.select = function(value, option) {
            self.editable = (value == '' ? true : false);
            if (self.editable) {
                $timeout(function() {
                    jQuery('#language>input').attr('placeholder', '语言名或扩展名').val('').focus();
                });
            }
            editor.getSession().setMode('ace/mode/' + option.mode);
        }

        self.submit = function (valid) {
            if (valid) {
                $uibModalInstance.close({ lang: self.lang, code: self.code });
            }
        }

        self.resize = function(height, width, fullscreen) {
            if (fullscreen) {
                jQuery('#source-editor').height(height - 100);
            } else {
                jQuery('#source-editor').height(500);
            }
            $timeout(function() {
                editor.resize();
            }, 50);
        }

        self.cancel = function () {
            $uibModalInstance.dismiss(false);
        }
    });
});
