# Cloudnote 牧云笔记

## 项目简介

Cloudnote牧云笔记 —— 基于ACE、Summernote、Handsontable和ZRender的在线云笔记，支持文本（富文本）、表格和绘图等几大功能。操作界面和风格与本地的编辑器相类似，基本上与用户日常办公的操作习惯一致。各类文件的操作都保存了历史版本，更好地为用户提供了文件的版本管理。

在线示例地址：http://47.92.196.8:8080/

## 安装运行

1. 安装[Node.js](https://nodejs.org/zh-cn/)

2. 在控制台执行以下命令：

  &gt; npm install

  &gt; npm start

> 若运行压缩版，请将config.js中的debug置为false，然后执行`npm run build`，再执行`npm start`。

## 使用说明

整个操作界面分为菜单栏、工具栏、侧边栏、工作区域、状态栏。侧边栏与工作区域可拖动分隔线来改变大小。**用户需要先新建模块后，在模块的基础上新建文件夹和文件。**_粘贴按钮由于javascript访问本地剪贴板受限，暂不可使用，可以通过快捷键【Ctrl+V】来粘贴。_ 操作界面如图：

![Cloudnote](https://images.gitee.com/uploads/images/2021/0227/114853_daf36a23_409346.png "cloudnote.png")

### 1. 菜单栏

- 文件：文件管理的相关操作

- 编辑：文件编辑的相关操作

- 视图：操作界面的显示设置

- 工具：ACE编辑器的相关配置

- 帮助：快捷键提示和用户账号

### 2. 工具栏

- 提供用户对操作界面、文件管理以及文件编辑的快捷操作。

### 3. 侧边栏

- 展示文件目录，可折叠、展开和隐藏，**双击可打开文件**。

- **点击右键会弹出菜单**，然后可以点击菜单进行相应操作。

### 4. 工作区域

- 文件面板会列出当前已打开的文件，**面板上的文件列表可进行移动，双击可切换工作区域全屏展示，右键可点击关闭等操作**。

- 文件面板右侧有下拉文件列表，**可过滤查找或选择打开其中一个文件**。

- 编辑区域可选择工作栏上的工具进行编辑，**也可右键菜单选择相应的操作，还可以通过快捷键来操作**。

### 5. 状态栏

- 展示当前编辑文件的相关状态

## 界面截图

### Summernote（文档）

![Summernote](https://images.gitee.com/uploads/images/2021/0227/114925_52670b50_409346.png "summernote.png")

### Handsontable（表格）

![Handsontable](https://images.gitee.com/uploads/images/2021/0227/114950_3b4d8341_409346.png "handsontable.png")

### ZRender（图形）

![ZRender](https://images.gitee.com/uploads/images/2021/0227/115013_7a516e06_409346.png "zrender.png")
