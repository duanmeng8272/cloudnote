define([
    'config',
    'js/resolve',
    'js/resources',
    'js/init',
    'rcss!css/default'
], function(config, resolve, resources) {
    var app = angular.module('app', ['app.main']);

    app.config(function($httpProvider, cfpLoadingBarProvider) {
        // loadingbar settings
        cfpLoadingBarProvider.includeBar = true;
        cfpLoadingBarProvider.includeSpinner = false;

        // http interceptor
        $httpProvider.interceptors.push(function($injector, $rootScope, $cookies) {
            return {
                responseError: function(rejection) {
                    // error
                    if (rejection.status == -1) {
                        $injector.get('_dialog').alert('错误信息', '服务器已断开连接或连接超时！');
                    } else if (rejection.status == 401) {
                        $rootScope.sessionUser = {};
                        $cookies.remove('session_user');
                        $injector.get('_dialog').open().security();
                    } else if (rejection.status == 403) {
                        $injector.get('_dialog').alert('提示信息', '您没有权限，请联系管理员！');
                    } else if (rejection.status >= 400) {
                        $injector.get('_dialog').alert('错误信息', rejection.data);
                    }
                    return rejection;
                }
            };
        });
    });

    app.config(function($controllerProvider, $routeProvider, $compileProvider, $filterProvider, $provide) {
        // register functions
        app.provider = $provide.provider;
        app.controller = $controllerProvider.register;
        app.directive = $compileProvider.directive;
        app.filter = $filterProvider.register;
        app.factory = $provide.factory;
        app.service = $provide.service;

        // route
        angular.forEach(resources.pages, function(page) {
            $routeProvider.when(page.url, resolve(angular.extend({
                controllerAs: 'vm'
            }, page)));
        })
        $routeProvider.otherwise({
            redirectTo: config.defaultPath
        });
    });

    app.run(function($http, $rootScope, $route, $location, $cookies, _dialog) {
        // disable context menu
        if (config.disableContextmenu) {
            jQuery(document).on('contextmenu', function(event) {
                return false;
            });
        }

        $rootScope.accessible = false;
        // session user
        var sessionUser = $cookies.getObject('session_user');
        if (sessionUser) {
            $rootScope.sessionUser = sessionUser;
            $rootScope.accessible = true;
        }
        // check and reload session user
        $http.get('/note/auth/rememberme').then(function(response) {
            if (response.status == 200) {
                sessionUser = response.data;
                $rootScope.accessible = response.data['accessible'];
                if (!response.data['accessible']) {
                    _dialog.open().security();
                }
            }
            // update session user
            $rootScope.sessionUser = sessionUser;
            if (sessionUser.email) {
                $cookies.putObject('session_user', sessionUser);
            } else {
                $cookies.remove('session_user');
            }
        });

        // override location path
        var original = $location.path;
        $location.path = function(path, reload) {
            if (reload === false) {
                var lastRoute = $route.current;
                var un = $rootScope.$on('$locationChangeSuccess', function() {
                    $route.current = lastRoute;
                    un();
                });
            }
            return original.apply($location, [path]);
        };
    });

    angular.bootstrap(document, ['app']);

    return app;
});
