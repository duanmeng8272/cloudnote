/**
 * Note email template
 * @author Wangtd
 */
const ejs = require('ejs');

const templates = {
    register_kaptcha: [
        '<p>亲爱的：<p>',
        '<p style="text-indent:2em">您注册的验证码为：<font color="green"><%=kaptcha%></font>，请及时处理！</p>',
        '<br>',
        '<hr>',
        '<p>系统邮件，请勿回复！</p>'
    ].join('\n'),
    reset_password: [
        '<p>亲爱的：<p>',
        '<p style="text-indent:2em">您重置后的密码为：<font color="red"><%=password%></font>，请妥善保管！</p>',
        '<br>',
        '<hr>',
        '<p>系统邮件，请勿回复！</p>'
    ].join('\n')
};

module.exports.render = function(name, data, options) {
    return ejs.render(templates[name], data, options);
}
