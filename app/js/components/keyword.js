define(['angular-sanitize'], function() {
    angular.module('app.keyword', ['ngSanitize'])
        .filter('keyword', keywordFilter);

    function keywordFilter($sce) {
        return function(items, keyword, name) {
            var newItems = [];
            if (keyword) {
                var rkeyword = new RegExp(keyword, 'gi');
                angular.forEach(items, function(item) {
                    var text = item[name];
                    if (rkeyword.test(text)) {
                        var highlight = text.replace(rkeyword,
                            '<span style="color:whitesmoke;background-color:darkgrey;">$&</span>');
                        item['highlight'] = $sce.trustAsHtml(highlight);
                        newItems.push(item);
                    }
                });
            } else {
                angular.forEach(items, function(item) {
                    item['highlight'] = $sce.trustAsHtml(item[name]);
                    newItems.push(item);
                });
            }
            return newItems;
        }
    }
});
