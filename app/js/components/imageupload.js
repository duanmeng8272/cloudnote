define([
    'js/resolve',
    'js/components/dialog'
], function(resolve) {
    angular.module('app.imageupload', ['app.dialog'])
        .directive('imageupload', imageUploadDirective);

    function imageUploadDirective(_dialog) {
        return resolve({
            restrict: 'E',
            replace: true,
            template: '<button class="btn btn-default" title="图片" ng-click="select()"><i class="fa fa-image"></i></button>',
            scope: {
                onSelected: '<'
            },
            link: function(scope) {
                scope.select = function() {
                    _dialog.open().uploadImage().then(function(image) {
                        if (typeof(scope.onSelected) === 'function') {
                            scope.onSelected(image);
                        }
                    });
                }
            }
        });
    }
});
