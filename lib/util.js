/**
 * Note Utility
 * @author Wangtd
 */
const fs = require('fs');
const path = require('path');
const util = require('util');

module.exports = {
    each: function(items, callback) {
        if (items && items.length > 0) {
            for (var index in items) {
                callback(items[index], index);
            }
        }
    },
    map: function(items, mapping) {
        var mappings = [];
        this.each(items, function(item, index) {
            mappings.push(mapping(item, index));
        });
        return mappings;
    },
    find: function(items, predicate) {
        if (items && items.length > 0) {
            for (var index in items) {
                if (predicate(items[index], index)) {
                    return items[index];
                }
            }
        }
        return null;
    },
    path: function() {
        return path.join.apply(this, arguments);
    },
    paths: function() {
        var args = [path.join(__dirname, '../')];
        for (var i = 0; i < arguments.length; i++) {
            args.push(arguments[i]);
        }
        return path.join.apply(this, args);
    },
    random: function(size) {
        const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        const length = chars.length - 1;
        var i = -1, out = '';
        while (++i < size) {
            out += chars.charAt(Math.round(Math.random() * length));
        }
        return out;
    },
    format: function() {
        return util.format.apply(this, arguments);
    },
    mkdirsSync: function(dirname) {
        if (fs.existsSync(dirname)) {
            return true;
        } else {
            if (this.mkdirsSync(path.dirname(dirname))) {
                fs.mkdirSync(dirname);
                return true;
            }
        }
    },
    readFileSync: function(filepath) {
        return fs.readFileSync(filepath, 'utf-8');
    },
    writeFileSync: function(filepath, text, overwritten) {
        if(fs.existsSync(filepath)) {
            if (!overwritten) return;
        } else {
            this.mkdirsSync(path.dirname(filepath));
        }
        fs.writeFileSync(filepath, text, { flag: 'w', encoding: 'utf-8' });
    },
    copyFileSync: function(targetpath, sourcepath, overwritten) {
        this.writeFileSync(targetpath, this.readFileSync(sourcepath), overwritten);
    },
    deleteAllSync: function(filepath) {
        var _this = this;
        if(fs.existsSync(filepath)) {
            if(fs.statSync(filepath).isDirectory()) {
                var files = fs.readdirSync(filepath);
                for (var i in files) {
                    var filename = files[i];
                    var curpath = path.join(filepath, filename);
                    if(fs.statSync(curpath).isDirectory()) {
                        _this.deleteAllSync(curpath);
                    } else {
                        fs.unlinkSync(curpath);
                    }
                }
                fs.rmdirSync(filepath);
            } else {
                fs.unlinkSync(filepath);
            }
        }
    }
};
