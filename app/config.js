define(function() {
    return {
        baseUrl: '/app/',
        defaultPath: '/:path*?',
        disableContextmenu: true
    };
});
