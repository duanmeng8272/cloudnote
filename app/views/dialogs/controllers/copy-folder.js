define(['app'], function(app) {
    app.controller('copyFolder', function($http, _context, _dialog, $uibModalInstance, toastr) {
        var self = this;

        self.init = function() {
            self.exclude = '/' + _context.path;
            self.source = _context.path;
            self.directory = '';
            self.target = '';
            self.ignored = true;
            self.movable = false;
        }

        self.select = function(path) {
            self.directory = path;
        }

        self.submit = function(valid) {
            if (valid) {
                var slashIndex = self.directory.indexOf('/');
                var module = self.directory.substring(0, slashIndex);
                var target = self.directory.substring(slashIndex + 1) + '/' + self.target;
                // copy folder
                $http.post('/note/data/copy', {
                    type: 'folder',
                    module: module,
                    source: self.source,
                    target: target,
                    ignored: self.ignored,
                    movable: self.movable
                }).then(function(response) {
                    setTimeout(function() {
                        if (response.data == 'required') {
                            _dialog.alert('提示信息', '模块【' + module + '】不存在！');
                        } else if (response.status == 200) {
                            toastr.success('文件夹【' + self.target + '】已复制！');
                            $uibModalInstance.close(true);
                        }
                    }, 500);
                });
            }
        }

        self.cancel = function() {
            $uibModalInstance.dismiss();
        }
    });
});
