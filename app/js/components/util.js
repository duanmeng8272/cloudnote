define(['clipboard'], function(Clipboard) {
    var utilFactory = angular.module('app.util', []);

    var clipboard = null;

    utilFactory.factory('_util', function() {
        return {
            copy: function(text) {
                var button = jQuery('#clipboard-copy');
                if (button.length == 0) {
                    button = jQuery('<button id="clipboard-copy" class="hidden"></button>');
                    button.appendTo(document.body);
                    clipboard = new Clipboard('#clipboard-copy', {
                        text: function() {
                            return text;
                        }
                    });
                } else {
                    clipboard.text = function() {
                        return text;
                    };
                }
                button.click();
            }
        };
    });
});
