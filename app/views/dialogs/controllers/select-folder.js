define(['app'], function(app) {
    app.controller('selectFolder', function($uibModalInstance) {
        var self = this;

        self.init = function() {
            self.directory = '';
        }

        self.select = function(path) {
            self.directory = path;
        }

        self.submit = function(valid) {
            if (valid) {
                $uibModalInstance.close(self.directory);
            }
        }

        self.cancel = function() {
            $uibModalInstance.dismiss();
        }
    });
});
