define(['app', 'moment'], function(app, moment) {
    app.controller('security', function($rootScope, $scope, $http, $uibModalInstance, $cookies, $interval,
            $window, toastr) {
        var self = this;
        self.modalType = 'login';
        self.randomId = 0;
        self.email = '';
        self.password = '';
        self.rememberMe = true;
        self.cfmPassword = '';
        self.captcha = '';

        self.kaptcha = '';
        self.kaptchaEnabled = true;
        self.kaptchaTimer = '';

        self.init = function() {
            self.changeRandomId();
            $scope.$watch('dvm.modalType', function() {
                self.email = '';
                self.password = '';
                self.rememberMe = true;
                self.captcha = '';
                self.kaptcha = '';
            });
        }

        self.changeRandomId = function() {
            self.randomId = Math.random();
        }

        self.getKaptcha = function() {
            self.kaptchaEnabled = false;
            self.kaptchaTimer = '（60秒）';
            // send email captcha
            $http.get('/note/auth/kaptcha?email=' + self.email).then(function(response) {
                if (response.status == 200) {
                    toastr.success('注册验证码已发送！');
                }
            });
            // enabled after 60s
            var count = 60;
            $interval(function() {
                count--;
                if (count == 0) {
                    self.kaptchaEnabled = true;
                    self.kaptchaTimer = '';
                    return;
                }
                self.kaptchaTimer = '（' + count + '秒）';
            }, 1000, 60);
        }

        self.login = function(valid) {
            if (valid) {
                $http.post('/note/auth/login', {
                    email: self.email,
                    password: self.password,
                    rememberMe: self.rememberMe
                }).then(function(response) {
                    if (response.data == 'user') {
                        toastr.error('邮箱或密码不正确！');
                    } else if (response.status == 200) {
                        $uibModalInstance.close(true);
                        $rootScope.sessionUser = response.data;
                        $cookies.putObject('session_user', response.data, {
                            expires: moment().add(30, 'minutes').toDate()
                        });
                        toastr.success('登录成功！');
                        if ($rootScope.accessible === false) {
                            $window.location.reload();
                        }
                    }
                });
            }
        }

        self.register = function(valid) {
            if (valid && self.cfmPassword == self.password) {
                $http.post('/note/auth/register', {
                    email: self.email,
                    password: self.password,
                    kaptcha: self.kaptcha
                }).then(function(response) {
                    if (response.data == 'locked') {
                        toastr.warning('已有用户在编辑，请稍候再试！');
                    } else if (response.data == 'kaptcha') {
                        toastr.error('验证码不正确！');
                    } else if (response.data == 'domain') {
                        toastr.error('非企业内部邮箱！');
                    } else if (response.data == 'email') {
                        toastr.error('邮箱已存在！');
                    } else if (response.status == 200) {
                        self.modalType = 'login';
                        toastr.success('注册成功，请登录！');
                    }
                });
            }
        }

        self.reset = function(valid) {
            if (valid) {
                $http.post('/note/auth/reset', {
                    email: self.email,
                    captcha: self.captcha
                }).then(function(response) {
                    if (response.data == 'locked') {
                        toastr.warning('已有用户在编辑，请稍候再试！');
                    } else if (response.data == 'captcha') {
                        self.changeRandomId();
                        toastr.error('验证码不正确！');
                    } else if (response.data == 'email') {
                        toastr.error('邮箱不存在！');
                    } else if (response.status == 200) {
                        self.modalType = 'login';
                        toastr.success('密码重置成功，请登录！');
                    }
                });
            }
        }

        self.close = function() {
            $uibModalInstance.close();
        }
    });
});
