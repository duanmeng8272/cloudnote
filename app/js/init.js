define([
    'moment',
    'angular-route',
    'angular-cookies',
    'angular-hotkeys',
    'angular-loading-bar',
    'angular-toastr.tpls',
    'angular-ui-bootstrap',
    'ng-file-upload-shim',
    'js/components/combobox',
    'js/components/contextmenu',
    'js/components/winresize',
    'js/components/filelist',
    'js/components/dialog',
    'js/components/dirtree',
    'js/components/aceeditor',
    'js/components/markdown',
    'js/components/summernote',
    'js/components/handsontable',
    'js/components/zrender'
], function(moment) {
    angular.module('app.main', [
        'ngRoute',
        'ngCookies',
        'cfp.hotkeys',
        'angular-loading-bar',
        'toastr',
        'ui.bootstrap',
        'ngFileUpload',
        'app.combobox',
        'app.contextmenu',
        'app.winresize',
        'app.filelist',
        'app.dialog',
        'app.dirtree',
        'app.aceeditor',
        'app.markdown',
        'app.summernote',
        'app.handsontable',
        'app.zrender'
    ])
    .config(function($cookiesProvider, $qProvider, toastrConfig) {
        // jQuery
        (function($) {
            var oldSetOption = $.ui.resizable.prototype._setOption;
            $.ui.resizable.prototype._setOption = function(key, value) {
                oldSetOption.apply(this, arguments);
                if (key === "aspectRatio") {
                    this._aspectRatio = !!value;
                }
            };
        })(jQuery);

        // cookies
        $cookiesProvider.defaults.expires = moment().add(30, 'days').toDate();

        // $q
        $qProvider.errorOnUnhandledRejections(false);

        // angular-toastr config
        angular.extend(toastrConfig, {
            autoDismiss: false,
            containerId: 'toast-container',
            maxOpened: 0,
            newestOnTop: false,
            positionClass: 'toast-bottom-right',
            preventDuplicates: false,
            preventOpenDuplicates: true,
            target: 'body',
            timeOut: 5000
        });
    })
    // off autocomplete
    .directive('form', function() {
        return {
            restrict: 'E',
            link: function(scope, element) {
                element.attr('autocomplete', 'off');
            }
        };
    })
    // autofocus
    .directive('autofocus', function() {
        return {
            restrict: 'A',
            link: function(scope, element) {
                element[0].focus();
            }
        };
    });
});
