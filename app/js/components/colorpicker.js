define([
    'js/resolve',
    'angular-ui-bootstrap',
    'angular-color-picker'
], function(resolve) {
    angular.module('app.colorpicker', ['ui.bootstrap', 'color.picker'])
        .directive('colorpicker', colorPickerDirective);

    function colorPickerDirective($timeout) {
        return resolve({
            restrict: 'E',
            replace: true,
            templateUrl: 'js/templates/colorpicker.html',
            scope: {
                forecolor: '=',
                backcolor: '=',
                position: '<',
                icon: '<',
                colorTips: '<',
                forecolorTips: '<',
                backcolorTips: '<',
                onSelected: '<'
            },
            link: function(scope, element, attrs) {
                scope.$forecolor = scope.forecolor;
                scope.$backcolor = scope.backcolor;
                scope.colorStyle = { 'color': scope.$forecolor };
                if (scope.$backcolor) {
                    scope.colorStyle['background-color'] = scope.$backcolor;
                }
                //
                var options = {
                    inputClass: 'hidden',
                    allowEmpty: true,
                    format: 'rgb',
                    case: 'lower',
                    hue: true,
                    saturation: false,
                    lightness: false,
                    alpha: true,
                    swatch: false,
                    swatchOnly: false,
                    swatchBootstrap: false,
                    inline: true,
                    horizontal: true,
                    restrictToFormat: true,
                    preserveInputFormat: true
                };
                scope.options = {
                    forecolor: angular.extend({
                        placeholder: scope.forecolorTips || '前景色'
                    }, angular.copy(options)),
                    backcolor: angular.extend({
                        placeholder: scope.backcolorTips || '背景色'
                    }, angular.copy(options))
                };
                scope.event = {
                    forecolor: {
                        onChange: function(api, color) {
                            if (color) {
                                scope.forecolor = color;
                                scope.colorStyle['color'] = color;
                            } else {
                                api.clear();
                                scope.forecolor = null;
                                delete scope.colorStyle['color'];
                            }
                            scope.select();
                        }
                    },
                    backcolor: {
                        onChange: function(api, color) {
                            if (color) {
                                scope.backcolor = color;
                                scope.colorStyle['background-color'] = color;
                            } else {
                                api.clear();
                                scope.backcolor = null;
                                delete scope.colorStyle['background-color'];
                            }
                            scope.select();
                        }
                    }
                };

                $timeout(function() {
                    element.find('.dropdown-menu>li').bind('click', function(event) {
                        return false;
                    });
                }, 500);

                scope.select = function() {
                    if (typeof(scope.onSelected) === 'function') {
                        scope.onSelected(scope.forecolor, scope.backcolor);
                    }
                }
            }
        });
    }
});
